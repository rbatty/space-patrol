*************************
***** SPACE PATROL! *****
* Adventures in Numbers *
*************************

An html5/canvas game by Rachel Ponce (rbattybits@gmail.com).
A side-scrolling space shooter and math game. Currently with 10 stages.
Playable version online at: http://www.offbeatgames.com/webgames.html


CURRENT RELEASE: v1.1-beta-1, August 16, 2013


ATTRIBUTION

Spaceships and scrolling background art:
http://opengameart.org/content/simple-shoot-em-up-sprites-spaceship-starscape-ufo-0

Explosion art: http://opengameart.org/content/i-are-spaceship-16x16-space-sprites

Background music sampled from: "Shapeshifter" by Clearside
https://soundcloud.com/clearside/clearside-shapeshifter

Shooting sounds: "63 Digital sound effects " by www.Kenney.nl
http://opengameart.org/content/63-digital-sound-effects-lasers-phasers-space-etc

Additional sound effects: "FPS Placeholder Sounds" by Jack Menhorn
http://opengameart.org/content/fps-placeholder-sounds

Preloader script: html5Preloader.js by jussi-kalliokoski
https://github.com/jussi-kalliokoski/html5Preloader.js

All artwork and libraries used are included under a Creative Commons/MIT license and are free for non-commercial use.


FUTURE FEATURES LIST
- Improve graphics and styling of intro and story screens
- More enemy types and improve boss AI in later levels
- Boss images for dialog scenes
- Better breakdown of player performance after each level


POSSIBLE FEATURES TO BE ADDED
- Health power ups for player
- Weapon power-ups for player


RELEASE NOTES

v1.1-beta-1 - Rewrites sound preloading script and fixes asset-loading bug. Game should no longer intermittently hang on load.

v1.0-beta-1 - First beta release. All 10 levels playable.

v0.1 - First alpha release of math-based version. Works with most current releases of Chrome, Firefox, and Safari.


KNOWN BUG LIST

    General
    -None.

    Browser-specific
    - Safari 5.1.7 and earlier throws an error regarding CustomEvent objects.
