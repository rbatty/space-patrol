rachelnponce.spacepatrol.Stars = function(velocity) {
    this.color = getColor();
    this.opacity = 1;
    this.shadow = "white";
    this.blur = 8;
    this.x = 50;
    this.y = -100;
    this.radius = 3;
    this.vx = Math.random()*0.95;
    
    if (velocity) {
       this.vx = (Math.random()*( velocity-1 )) + 0.5; //velocity at least .5
    }

    function getColor() {
        var c = Math.floor(Math.random()*5);
        var color = "";

        switch (c) {
            case 0:
               color = "179,243,252,";
               break;
            case 1:
               color = "252,246,179,";
               break;
            case 2:
            case 3:
            case 4:
            default:
                color = "255,255,255,";
                break;
        }
        return color;
    }

    this.update = function(width) {
        this.x -= this.vx;
        if (this.x < -10 ) {
            this.x = width + 10;
        }
    };

    this.draw = function(ctx) {
        var myCtx = ctx;
        myCtx.beginPath();
        //you can have squares instead of circles
        myCtx.arc(this.x,this.y,this.radius,0,2*Math.PI);
        //myCtx.rect(this.x, this.y, this.radius, this.radius);
        myCtx.fillStyle = "rgba("+this.color+this.opacity+")";
        myCtx.shadowBlur = this.blur;
        myCtx.shadowColor= this.shadow;
        myCtx.fill();
    };
}