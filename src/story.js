rachelnponce.spacepatrol.Story = function(listener) {
	var _listener = listener;
	var needNext = false;   //for inserting clickable next button
	var textArea = document.getElementById('textOverlay');

	this.printText = function(txt) {
    	//get the right text
    	var storyData = textPicker(txt);
    	textArea.innerHTML = storyData[0];  //text only

	    if (needNext) {
	        nextButton();
	        needNext = false;
	    }
	}

	this.returnText = function(txt) {
		return textPicker(txt); //array with text and image
	}

    function textPicker(txt) {
        var temp = "<div class = 'story'>";
        var pict = null;
        switch (txt) {
        	case "mainmenu":
        		temp = "<h2>SPACE PATROL!</h2>";
        		break;
        	case "level0":
        		temp += "<p>Space Patrol Agent 2137:</p>" +
        		"<p>So, kid, you think you're good enough to join the galaxy's finest police force, huh? Well, we'll just have to see about " +
        		"that. I don't usually send rookies out alone on the first day, but we're a little short-handed at the moment. I'm sending " +
        		"you to the Donut Cluster, where you'll need to keep an eye out for a space gang that call themselves The Zeroes. They're a " +
        		"nasty bunch and they've been terrorizing the law abiding citizens in that area. Take 'em out, or go home!</p>" +
        		"<p>-The Chief</p>";
        		var extra = "<p class='instruction'>&lt;press space to continue&gt;</p>";
                break;
            case "level0boss":
            	temp += "<p class='bossname'>Alexandr&eacute;:<br />" +
            	"<p><i>Zut alors!</i> Where did you come from, Space Patrol? And what have you done to all my Zeroes? You'll pay for " +
            	"this! Wipe that smug look off your face--you think I am not serious? Listen here, Space Patrol, I used to be a Pilates " +
            	"instructor. I know all about pain! I will make you suffer!!</p>" +
        		"<p class='instruction'>&lt;press space to continue&gt;</p>";
                pict = rachelnponce.spacepatrol.game.getAssets('boss1');
            	break;
        	case "level1":
        		temp += "<p>Hey there, Rookie,<br />" +
        		"Good job taking down that exercise-instructor-turned-criminal-mastermind, Alexandr&eacute;! " +
        		"<p>For your next assignment, I need you to take out some thugs who have hijacked a bunch of top-of-the-line personal assault " +
        		"ships in the Delta Sector. Problem is, they're not the only ships around. Only take out the thugs' ships and spare the " +
        		"civilians, you hear? You can recognize the thugs by the numbers on their ships. All the thug's ships have <strong>multiples " +
        		"of 2</strong> on them.  Bring those thugs down, rookie, and maybe you'll earn my respect. </p>" +
        		"<p>-The Chief</p>";
        		break;
            case "level1boss":
            	temp += "<p class='bossname'>Tum Tum:<br />" +
            	"Tum Tum angry! Why you hurt all Tum Tum's little friends? Tum Tum hate Space Patrol. TUM TUM SMAAAAAAAASH!!!!!</p>" +
            	"<p class='instruction'>&lt;press space to continue&gt;</p>";
                //pict = rachelnponce.spacepatrol.game.getAssets('boss2');
            	break;
        	case "level2":
        		temp += "<p>Pretty good shooting, Rookie. Keep that up and you may just make Sergeant before you're 50! " +
        		"Since you did such a good job, I'm putting you on a more challenging assignment. There's a group of space pirates " +
        		"who are destroying all our surveillance satellites. This crew is a bit more aggressive than you may be used to. Watch out "+
        		"for their leader in particular. She won't hesitate to mow you down. Approach with extreme caution!</p>" +
        		"<p>But don't just go shooting everything in sight! You can recognize the pirate ships by the numbers on their ships. Only " +
        		"ships with <strong>multiples of 3</strong> on them belong to the pirates.</p>";
        		break;
        	case "level2boss":
        		temp = "<p class='bossname'>Roller Queen:<br />" +
        		"Space Patrol? Now why did you have to go and ruin our fun! Sure, my crew was a little clumsy and knocked a few " +
        		"satellites about, but really, there's too much space clutter in the first place. And now I have no one to practice " +
        		"all my awesome moves with. Hmm, I guess I'll just have to make you my practice partner. See if you can keep up, " +
        		"dearie, because if not, I'll just have to run you down!</p>" +
        		"<p class='instruction'>&lt;press space to continue&gt;</p>";
                //pict = rachelnponce.spacepatrol.game.getAssets('boss3');
        		break;

        	case "level3":
        		temp = "<p>That was a nice job you did rolling over the Roller Queen. Now, go stop the Numero Quatro Gang. They're pretty easy " +
        		"to recognize--they've got <strong>multiples of the number 4</strong> painted on all their ships. </p>";
        		break;
        	case "level3boss":
        		temp = "<p class='bossname'>The Great Rovanelli:<br />" +
        		"Cut! Cut! Who are you? You're ruining my shots! Can't you see I'm trying to film a documentary here? Do you know how long " +
        		"it took me to find a space gang that would let me film them? Now what am I supposed to do? What? You want to confiscate " +
        		"my footage now, too? Never! You want it, you're going to have to take it by force. And my handy satellite camera is going" +
        		"to capture everything. Best documentary ever!!</p> " +
                "<p class='instruction'>&lt;press space to continue&gt;</p>";
                //pict = rachelnponce.spacepatrol.game.getAssets('boss4');
        		break;

        	case "level4":
        		temp = "<p>Thanks for stopping that crazy filmmaker. I wish I could say that your work is done, but it turns out there's" +
        		"some crazy lady planting bombs all over the Turing-McCulloch Sector. I need you to go and stop her. <strong>multiples of 5" +
        		"</strong> </p>";
        		break;
        	case "level4boss":
        		temp = "<p class='bossname'>Mad Marcie:<br />" +
        		"Ugh, Space Patrol. I don't have time for you. I'm going to blow you up now.</p>" +
        		"<p class='instruction'>&lt;press space to continue&gt;</p>";
                //pict = rachelnponce.spacepatrol.game.getAssets('boss5');
        		break;

        	case "level5":
        		temp = "<p>Thank you for stopping Mad Marcie. Now, we've got reports of some robot ships going haywire. For whatever reason, " +
        		"the robot ships have <strong>multiples of the number 6</strong> painted on them. Just stop the robots, whatever it takes!</p>";
        		break;
        	case "level5boss":
        		temp = "<p class='bossname'>Mr. Lin:<br />" +
        		"Well, well, Space Patrol. Is there a problem here? Oh, you don't like my drone ships? But they're such sophisticated " +
        		"devices. It took me weeks to program their A.I.! Why did you go and destroy them all? Hmph, well maybe it's time to update " +
        		"your ship's A.I. This is going to be fun!</p>" +
        		"<p class='instruction'>&lt;press space to continue&gt;</p>";
                //pict = rachelnponce.spacepatrol.game.getAssets('boss6');
        		break;

        	case "level6":
        		temp = "Wow, great job taking out that crazy hacker and his drone ships. You're a better pilot than I thought. Okay, hotshot, " +
        		"I need you to go handle a problem with a crazy underground cult. The group calls themselves The Rayzers and they seem to be " +
        		"harvesting all kinds of lifeforms for some sinister experiments. Their leader is some kind of mad doctor who likes cutting " +
        		"things up. They're going to be a handful, but I trust you can handle it. Rayzers like <strong>multiples of the number 7</strong>" +
        		" </p>";
        		break;
        	case "level6boss":
        		temp = "<p class='bossname'>Dr. Adam:<br />" +
        		"Well, well, what have we here? The Space Patrol, is it? You know, I really have no time for you. I have bodies to " +
        		"collect, dissections to perform. You have no idea how many species I've yet to examine, how many pathologies I've yet " +
        		"to uncover. You cannot stop the march of science, Space Patrol. No one can. Now get out of my way!</p>" +
        		"<p class='instruction'>&lt;press space to continue&gt;</p>";
                //pict = rachelnponce.spacepatrol.game.getAssets('boss7');
        		break;

        	case "level7":
        		temp = "<p>Great work! I don't know how we ever got by without you. Since you're so good, I'm hoping you can put an end to this "+
        		"freaky space vampire in the Halo Nebula. I\"m not sure what you can expect from him, but his followers are obsessed with "+ 
        		"<strong>the number 8</strong>, so hopefully they'll be easy for you to recognize.</p>";
        		break;
        	case "level7boss":
        		temp = "<p class='bossname'>Vampire Garrote:<br />" +
        		"Leave me alone, Space Patrol. I am a creature of the night, and space is the purest night. Just let me drift among the " +
        		"stars...<br />" +
        		"Ugh, I can see you aren't leaving. I am overdue for a little snack. Your ship's energy looks tasty enough.</p>" +
        		"<p class='instruction'>&lt;press space to continue&gt;</p>";
                //pict = rachelnponce.spacepatrol.game.getAssets('boss8');
        		break;

        	case "level8":
        		temp = "<p>Impressive work. Glad we don't have to deal with that vampire guy anymore. But there's a big commotion going down near "+
        		"Jupiter. Two crazy brothers kidnapped some guy's girlfriend and he went on a rampage trying to get her back. Neeless to say, " +
        		"the guy failed to resolve the situation and we need you clean up this mess. The Brothers have <strong>multiples of the number 9"+
        		"</strong> stenciled all over their gang's ships. </p>";
        		break;
        	case "level8boss":
        		temp = "<p class='bossname'>The Brothers:<br />" +
        		"Uh-oh, it's the Space Patrol. Didn't expect to see you out here. Did that wimp Alex call you? I swear, you kidnap a guy's " +
        		"girlfriend and challenge him to a space fight and what does the guy do? Call the cops? I miss the good old days when " +
        		"bad dudes could just fight each other for no reason. Oh well, I guess we've got to deal with you now.</p>" +
        		"<p class='instruction'>&lt;press space to continue&gt;</p>";
                //pict = rachelnponce.spacepatrol.game.getAssets('boss9');
        		break;

        	case "level9":
        		temp = "<p>I have to say, I never thought you'd last this long, kid, but you're doing a great job. I've got one final task for " +
        		"you. Get this right and I'll put you down for a promotion to Space Deputy, First Class. There's some maniac wrecking havoc " +
        		"in the Alpha Centauri system. Calls himself \"The Philosopher\". He's extremely dangerous and enamored with<strong> the number " +
        		"10</strong>. You need to stop him and his minions. Be careful, kid!</p>";
        		break;
        	case "level9boss":
        		temp = "<p class='bossname'>The Philosopher:<br />" +
        		"Well well, Space Patrol. Stop me if you've heard this one before: Descartes is sitting in a cafe, waiting on a friend. The " +
        		"waiter says, \"Can I get you something to drink?\" Descarte replies, \"I think not,\" and POOF! he disappears. Get it? Ha ha " +
        		"ha ha ha ha ha! Now, Space Patrol, make like Descartes and disappear! </p>" +
        		"<p class='instruction'>&lt;press space to continue&gt;</p>";
                //pict = rachelnponce.spacepatrol.game.getAssets('boss10');
        		break;

        	case "level10":
        		temp = "<p>You're good, kid. One of the best I've seen. I'm going to have to find a bigger challenge for you.</p>" +
        		"<p>To be continued...?</p>";
        		break;



            case "fail":
                temp += "<p>Well, Rookie, I expected a little more from you. You're lucky to be alive, ya know? I don't need to tell you what " +
                "would have happened if your emergency pod hadn't been picked up by a nearby salvage ship.</p>" +
                "<p>Here's a tip for you: you'll fare better out there if you try to <em>avoid</em> getting shot or crashing into other ships.</p> " +
                "<p>Now, do you think you can handle going back out there again?</p>";
                break;
            case "fail2":
                temp += "<p>Sorry kid, I can't afford to lose another patrol ship because of you. Those things don't grow on trees, ya " +
                "know?</p>" +
                "<p>You wanna keep flying for the space patrol, you're going to have to prove yourself again.";
                needNext = true;
                break;
            case "clear":
            default:
                temp="";
        }
        temp += "</div>";
        var storyElements = [];
        storyElements[0] = temp;
        storyElements[1] = pict;
        return storyElements;
    }

    function nextButton() {
        var next = document.createElement("a");
        next.id="next";
        next.innerHTML="Continue reading...";
        textArea.appendChild(next);
        next.onclick = onNext;
    }

    function onNext() {
       var evt = new CustomEvent("storyNext")
       _listener.dispatchEvent(evt);
    }
}

	/* boss list
	0 Alexandre - first
	1 Tum Tum - tank
	2 Roller Queen - charges with her ship
	3 The Great Rovanelli - film satellites
	4 Mr. Lin - hacker
	5 Mad Marcie - bombardier
	6 dr baim - lasers
	7 Mr Norlin - vampire/drain
	8 The Twins (jason & Trav) - synchronized flying
	9 dr tsou - no pattern/erratic?
	*/