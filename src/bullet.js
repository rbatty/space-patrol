rachelnponce.spacepatrol.Bullet = function(x, y, originator, playerX, playerY, name) {
    var self = this;
    this.x = x;
    this.y = y;
    this.type = "bullet";	//designate type of object for collision testing
    this.effect = "damage";	//collisions with this hurt the player
    this.pain = 1;  //how much damage does it do?
    this.origin = originator;	//tells who fired each bullet
    this.hit = false;	//tracks when a collsion with this bullet is registered
    if (name) {
    	this.name=name;
    }

    this.height = 4;
    this.width = 6;

    this.removeMe = false;	//change to true when it's time to remove this

    var _speed = 7.5;
    var _vx = 1.0;
    var _vy = 0.0;
    var _playerX = playerX;
    var _playerY = playerY;
    var color = "yellow";


    //calculate targeted velocities for enemies shooting at player
    if (this.origin === "enemy") {
    	enemyBullet(this);
    	switch (this.name) {
    		case "grunt3":	//speedy
    			_speed = 4.6;
    		default:
    			//do nothing
    			break;
    	}
    }
    
    if (this.origin === "boss") {
    	enemyBullet(this);
    	switch (this.name) {
    		case "alexandre":
    			_speed = 4.2;
    			break;
    		case "tumtum":
    			this.width = 8;
    			this.height = 8;
    			this.pain = 2;
    			break;
    		case "rollerqueen":
     			this.width = 4;
    			this.height = 4;
    			_speed = 8.5;
    			break;   			
    	}	
    	this.origin = "enemy" //for collision detection
    }

	this.draw = function(canvas) {
		var ctx = canvas.getContext('2d');

		newCoords = update(canvas);
		ctx.fillStyle = color;
        ctx.fillRect(newCoords[0], newCoords[1], self.width, self.height);
	}

    this.remove = function() {
        self.removeMe = true;
    }
    
    function enemyBullet(obj) {
    	//redefine the velocity & speed variables
        _speed = 3.2;
        _vx = _playerX - obj.x;
        _vy = _playerY - obj.y;
        var hyp = Math.sqrt( _vx * _vx + _vy * _vy );
        _vx /= hyp;
        _vy /= hyp;
        obj.height = 6; //make a bigger, more visible bullet for enemies
        color = "red";
    }

	function update(canvas) {
		var position = [];
		self.x += _vx * _speed;
		self.y += _vy * _speed;

   		position[0] = self.x;
   		position[1] = self.y
		return position;
	}

    return this;
};