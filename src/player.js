rachelnponce.spacepatrol.Player = function(width,height) {
    var self = this;
    var HEALTH = 6;	//3
    var POWER = 1;
    var HEIGHT = 29;
    var WIDTH = 64;
    var shipImage = rachelnponce.spacepatrol.game.getAssets('playerShip');
    var explodeImage = rachelnponce.spacepatrol.game.getAssets('explosion');

    this.x = 0.0;
    this.y = 0.0
    this.vx = 0.0;
    this.vy = 0.0;

    this.height = HEIGHT;
    this.width = WIDTH;
    this.type = "player";
    this.health = HEALTH;
    this.power = POWER;
    this.image = attachImage();
    this.firing = false;
    this.dead = false;

    var exploding = false;
    var bulletDelay = 250;  //in millisec
    var accel = 2.95;
    var speed = 5.0;
    var friction = 0.80;
    var cw = width;     //canvas width
    var ch = height;    //canvas height

    function clearedToFire() {
        self.firing = false;
    }

    function attachImage() {
	    var image = shipImage;
    	image.height = 29;
    	image.width = 64;
    	image.index = 0;
    	image.frameMax = 4;
    	image.xpos = 0;
    	image.ypos = 0;
    	return image;
    }

    this.delay = function(delay) {
        //delay is an optional parameter
        self.firing = true;
        if (delay) {
            var delayMe = delay;
        } else {
            var delayMe = bulletDelay;
        }
        setTimeout(clearedToFire,delayMe);
    }

	this.getFrame = function() {
		//spritesheet is stacked vertically - each frame needs new y position
		self.image.ypos = (self.image.height * self.image.index);
		self.image.index++;
		if (self.image.index >= self.image.frameMax) {
			self.image.index = 1;
		}
		return self.image.ypos;
	}

	this.update = function(action) {
		var position = [];
		if (action) {
            if (action.indexOf("left") > -1) {
                if (self.vx > -speed) {
                    self.vx-=accel;
                }
            }
            if (action.indexOf("right") > -1) {
                if (self.vx < speed) {
                    self.vx+=accel;
                }
            }

            if (action.indexOf("up") > -1) {
                if (self.vy > -speed) {
                    self.vy-=accel;
                }
            }
            if (action.indexOf("down") > -1) {
                if (self.vy < speed) {
                    self.vy+=accel;
                }
            }

            if (action.indexOf("shoot") > -1) {
                if (!self.firing) {
                    rachelnponce.spacepatrol.game.fire(self);
                    //limit firing rate
                    self.delay();
                }
            }

            if (action.indexOf("stop") > -1) {
                self.vx = 0;
                self.vy = 0;
            }
		}

		//friction & velocity make our movements smooth
		self.vy *= friction;
   		self.y += self.vy;

   		self.vx *= friction;
   		self.x += self.vx;

   		// keep it within canvas bounds
   		if (self.x >= cw-self.width) {
			self.x = cw-self.width;
		} else if (self.x <= 0) {
			self.x = 0;
		}

		if (self.y > ch-self.height) {
			self.y = ch-self.height;
		} else if (self.y <= 0) {
			self.y = 0;
		}

        //round it a bit - we don't need that much precision
        self.x = parseFloat(self.x).toFixed(2);
        self.y = parseFloat(self.y).toFixed(2);

   		position[0] = self.x;
   		position[1] = self.y
		return position;
	}

    this.explode = function() {
        this.image = explodeImage;
        this.image.xpos = 0;
        this.image.height = 15;
    	this.image.width = 15;
    	this.image.index = 0;
    	this.width = 50;
    	this.height = 50;
        explodeSound = rachelnponce.spacepatrol.game.getSoundAssets('explodeSound');
        explodeSound.volume = rachelnponce.spacepatrol.game.getEffectVolume();
        explodeSound.play();
    }

    this.upgrade = function(upType) {
        //stat boosting - forthcoming?
        switch (upType) {
            case "speed":
                speed++;
                break;
        }
    };

    this.updateHealth = function(amt) {
    	var currentHealth = this.health;
        this.health += amt;
        if (currentHealth > this.health) {
        	var hitSound = rachelnponce.spacepatrol.game.getSoundAssets('hit1Sound');
        	hitSound.volume = rachelnponce.spacepatrol.game.getEffectVolume();
			hitSound.play();
        }
        return this.health;
    }

    this.resetHealth = function() {
    	//corrects changes from explode animation
		this.height = HEIGHT;
    	this.width = WIDTH;

    	this.dead = false;
        this.health = HEALTH;
        return this.health;
    }

    return this;
};