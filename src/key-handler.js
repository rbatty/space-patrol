rachelnponce.spacepatrol.keyHandler = function(key) {
	var _action = [];
	_action.length = 0;
	var state = rachelnponce.spacepatrol.game.getState();

	if (key[81]) {
		// q
		_action.push("music");
	}
	
	if (key[90]) {
		// z
		_action.push("sound");	
	}

	if (state === "fighting") {
		if (key[37] || key[65]) {
			//left
			_action.push("left");
		}
		if (key[39] || key[68]) {
			//right
			_action.push("right");
		}
		if (key[38] || key[87]) {
			//up
			_action.push("up");
		}
		if (key[40] || key[83]) {
			//down
			_action.push("down");
		}
		if (key[32]) {
			//spacebar - fire
			_action.push("shoot");
		}
	} else if (state === "waitingForPlayer") {
		if (key[32]) {
			//spacebar
			_action.push("advance");
		}
	} else if (state === "waiting") {
        _action.push("stop");
    }

    return _action;
}
