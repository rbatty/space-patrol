rachelnponce.spacepatrol.Enemy = function(type,canvas) {
    var self = this;
    var _canvas = canvas;
    var enemyImage1 = rachelnponce.spacepatrol.game.getAssets('enemies1');
    var enemyImage2 = rachelnponce.spacepatrol.game.getAssets('enemies2');
    var enemyImage3 = rachelnponce.spacepatrol.game.getAssets('enemies3');
    var explodeImage = rachelnponce.spacepatrol.game.getAssets('explosion');
    var numberSheet = rachelnponce.spacepatrol.game.getAssets('numbers');

    this.name = '';
    this.type = 'enemy';	//designate special enemy types, like bosses
    this.effect = "damage";	//collisions with this hurt the player
    this.pain = 100; 	//high so collisions always kill the player
    this.id = '';
    this.integer = setInteger();	//each enemy's number
    this.image = attachImage(enemyImage1);
    this.width = 42;
    this.height = 36;
    this.x = _canvas.width +20;
    this.y = 0;
    this.vx = 2;
    this.vy = 0;
    this.a = 2;    //angle
    this.av = 0.1;    //angle velocity
    this.amp = 10;    //amplitude
    this.offset = 250;    //spacing between enemies in a wave
    this.onscreen = false;  //for regulating bullets & minimizing drawing
    this.reveal = true;
    this.exploding = false;
    this.value = 10;    //points player gets for destroying you
    this.color = "none";


    var health = 1;
    var shootInterval = (Math.random()*5000) + 2750;
	customize();
    this.onesImage = attachNumber();
    if (this.integer >= 10) {
    	this.tensImage = attachNumber();
    } else {
    	this.tensImage = null;
    }

    function customize() {
	    switch (type) {
	        case 1:     //wave pattern, bottom of screen, Zeros
	            self.name = "grunt1";
	            self.y = 288;
	            self.integer = 0;
	            self.offset = 200;
	            break;
	        case 2:     //wave pattern, top of screen, Zeros
	            self.name = "grunt2";
	            self.y = 72;
	            self.integer = 0;
	            self.offset = 200;
	            break;
	        case 3:     //wave pattern, bottom of screen
	            self.name = "grunt1";
	            self.y = 288;
	            break
	        case 4:     //wave pattern, top of screen
	            self.name = "grunt2";
	            self.y = 72;
	            break;
	        case 5:     //tight wave, midscreen
	            self.name = "grunt3";
	            self.image = attachImage(enemyImage2);
	            self.y = 180;
	            self.av = 0.2;
	            self.vx = 3.5;
	            break;
	        case 9:
	            self.name = "grunt1s";
	            self.y = 288;
	            self.offset = 0;
	            break;
	        case 10:
	            self.name = "grunt2s";
	            self.y = 72;
	            self.offset = 0;
	            break;
	        case 0.1:    //level 1 boss
	            self.name = "alexandre";
	            self.type = "boss";
	            self.integer = 0;
	            health = 10;
	            shootInterval = 2200;
	            self.width = 42;
	            self.height = 35;
	            self.x = 480;
	            self.y = 236;
	            self.vy = 4.5;
	            self.dir = 1;
	            self.value = 100;
	            self.reveal = false;
	            break;
	        case 11:    //level 2 boss
	            self.name = "tumtum";
	            self.image = attachImage(enemyImage2);
	            self.type = "boss";
	            self.integer = 2;
	            self.steps = 0;
	            self.phase = 0;
	            health = 20;
	            shootInterval = 1200;
	            self.width = 60;
	            self.height = 50;
	            self.x = 500;
	            self.y = 155;
	            self.a = 3; //2
	            self.vx = 2.5;
	            self.vy = 2.5;
	            self.value = 100;
	            self.reveal = false;
	            break;
	        case 21:	//level 3 boss
	        	self.name = "rollerqueen";
	            self.image = attachImage(enemyImage2);
	            self.type = "boss";
	            self.integer = 3;
	            self.phase = 0;
	            health = 12;
	            shootInterval = 3200;
	            self.width = 50;
	            self.height = 40;
	            self.x = 500;
	            self.y = 320;
	            self.newY = 0;
	            self.av = 0.05;
	            self.vx = 8;
	            self.vy = 1.5;
	            self.dir = -1;
	            self.value = 100;
	            self.reveal = false;
	            break;
	        case 31:	//level 4 boss
	        	self.name = "rovanelli";
	        	self.type = "boss";
	        	self.integer = 4;
	        	health = 25;
	        	shootInterval = 2500;
	            self.x = 500;
	            self.y = 160;
	            self.a = 3;
	            self.av = 0.05;
	            self.dir = 1;
	            self.offset = 0;
	        	self.value = 50;
	            self.reveal = false;
	            break;
	        case 32:
	        	self.name = "rovansat";
	        	self.image = attachImage(enemyImage3);
	        	self.type = "boss";
	        	self.integer = 4;
	        	health = 15;
	        	shootInterval = 2000;
	        	self.width = 30;
	            self.height = 28;
	        	self.x = 520;
	            self.y = 120;
	            self.a = 3;
	            self.aa = 2;
	            self.ava = 0.1;
	            self.av = 0.05;
	        	self.dir = 1;
		        self.offset = 0;
	        	self.value = 50;
	            self.reveal = false;
	            break;
	        case 41:    //level 1 boss
	            self.name = "alexandre";
	            self.type = "boss";
	            self.integer = 5;
	            health = 10;
	            shootInterval = 1800;
	            self.width = 42;
	            self.height = 35;
	            self.x = 480;
	            self.y = 236;
	            self.vy = 4.5;
	            self.dir = 1;
	            self.value = 100;
	            self.reveal = false;
	            break;
	        case 51:    //level 1 boss
	            self.name = "alexandre";
	            self.type = "boss";
	            self.integer = 6;
	            health = 10;
	            shootInterval = 1600;
	            self.width = 42;
	            self.height = 35;
	            self.x = 480;
	            self.y = 236;
	            self.vy = 4.5;
	            self.dir = 1;
	            self.value = 100;
	            self.reveal = false;
	            break;
	        case 61:    //level 1 boss
	            self.name = "alexandre";
	            self.type = "boss";
	            self.integer = 7;
	            health = 10;
	            shootInterval = 1400;
	            self.width = 42;
	            self.height = 35;
	            self.x = 480;
	            self.y = 236;
	            self.vy = 4.5;
	            self.dir = 1;
	            self.value = 100;
	            self.reveal = false;
	            break;
	        case 71:    //level 1 boss
	            self.name = "alexandre";
	            self.type = "boss";
	            self.integer = 8;
	            health = 10;
	            shootInterval = 1200;
	            self.width = 42;
	            self.height = 35;
	            self.x = 480;
	            self.y = 236;
	            self.vy = 4.5;
	            self.dir = 1;
	            self.value = 100;
	            self.reveal = false;
	            break;
	        case 81:	//level 4 boss
	        	self.name = "jason";
	        	self.image = attachImage(enemyImage2);
	        	self.type = "boss";
	        	self.integer = 9;
	        	health = 35;
	        	shootInterval = 1600;
	            self.x = 500;
	            self.y = 70;
	            self.vx = 2.4;
	            self.a = 3;
	            self.av = 0.05;
	            self.dir = 1;
	            self.ydir = 1;
	            self.offset = 0;
	        	self.value = 90;
	            self.reveal = false;
	            break;
	        case 82:
	        	self.name = "travis";
	        	self.image = attachImage(enemyImage2);
	        	self.type = "boss";
	        	self.integer = 9;
	        	health = 20;
	        	shootInterval = 800;
	        	self.x = 500;
	            self.y = 220;
	            self.vx = 2.4;
	            self.a = 3;
	            self.av = 0.05;
	        	self.dir = -1;
	        	self.ydir = -1;
		        self.offset = 0;
	        	self.value = 90;
	            self.reveal = false;
	            break;
	        case 91:    //level 1 boss
	            self.name = "alexandre";
	            self.type = "boss";
	            self.integer = 10;
	            health = 18;
	            shootInterval = 500;
	            self.width = 42;
	            self.height = 35;
	            self.x = 480;
	            self.y = 236;
	            self.vy = 4.5;
	            self.dir = 1;
	            self.value = 100;
	            self.reveal = false;
	            break;
	    }
    }

    function setInteger() {
    	/* assigns number values to enemy ships; roughly 3/5 will be multiples
    	 * keeps it fun and makes it easier the more players get to shoot
         * single digits in early levels, double digits for all others
         */
    	var level = rachelnponce.spacepatrol.game.getLevel();
    	var rand = 0;
    	if (level > 0) {
    		var temp = Math.floor(Math.random()*5);
    		if (temp < 2) {
                if (level > 1) {
                    rand = Math.floor(Math.random()*99) + 1;
                } else {
                    rand = Math.floor(Math.random()*9) + 1;
                }
    		} else {
                if (level > 1) {
                    rand = (Math.floor(Math.random()*11)+1) * (level +1);
                    if (rand >= 100) { rand = 99 };
                    console.log("multiple");
                } else {
                    rand = (Math.floor(Math.random()*5)+1) * (level + 1);
                }
    		}
    	} else {
    		rand = 0;
    	}

    	return rand;
    }

    // Once enemy is initialized, start the shooting w/random interval
    var fireInterval = setInterval(
        (function(slf) {
            return function() {
                slf.shoot(slf);
            }
        })(this),
        shootInterval
    );

    function attachImage(spriteSheet) {
        var image = spriteSheet;
    	image.height = 30;
    	image.width = 40;
    	image.index = 0;
    	image.frameMax = 6;
    	image.xpos = 0;
    	image.ypos = 0;
    	return image;
    }

    function attachNumber() {
    	var numberImg = numberSheet;
    	numberImg.width = 15;
    	numberImg.height = 20;
    	numberImg.ypos = 0;
    	numberImg.xpos = 0;
    	numberImg.scalew = 12;
    	numberImg.scaleh = 16;
    	numberImg.yoffset = 5;
    	return numberImg;
    }

    function getFrame() {
		self.image.ypos = (self.image.height * self.image.index);
		self.image.index++;
		if (self.image.index >= self.image.frameMax) {
			self.image.index = 1;
		}
		return self.image.ypos;
	}
	
	function updateNumber(that,place) {
		var num = that.integer;
    	var ones = num%10;
		var tens = ((num%100) - ones)/10;
    	//var hundreds = Math.floor(num/100);
    	var _place = 0;
    	if (place === "ones") {
    		_place = ones;
    	} else if (place === "tens") {
    		_place = tens;
    	}
    	that.onesImage.xpos = that.onesImage.width * _place;
    	//return that.onesImage.xpos;
    	//orange, red, green

    	if (that.integer < 10) {
    		that.onesImage.xoffset = Math.floor(that.width/2 - that.onesImage.scalew/2);
    	} else if (that.integer >= 10 && place === "ones") {
    		that.onesImage.xoffset = Math.floor(that.width/2);
    	} else if (that.integer >= 10 && place === "tens") {
    		that.tensImage.xoffset = Math.floor(that.width/2 - that.onesImage.scalew);
    	}
	}

	function setColor(numImg) {
		switch (self.color) {
			case "green":
				numImg.ypos = 20;
				numImg.yoffset -= 10;
				numImg.scaleh = 28;
				numImg.scalew = 20;
				break;
			case "red":
				numImg.ypos = 40;
				numImg.yoffset = 5;
				numImg.scalew = 12;
	    		numImg.scaleh = 16;
	    		break;
			default:
				numImg.ypos = 0;
				numImg.yoffset = 5;
				numImg.scalew = 12;
	    		numImg.scaleh = 16;
				break;
		}
	}

    this.draw = function(canvas) {
    	if (this.reveal === false) {
    		return;
    	}

        var ctx = canvas.getContext('2d');
        this.a += this.av;
        //define various movement patterns
        switch (this.name) {
            case "grunt1":
            case "grunt1s":
                sin = Math.sin(this.a);
                this.y += sin * 5;
                this.x -= this.vx;
                break;
            case "grunt2":
            case "grunt2s":
                sin = Math.sin(this.a);
                this.y -= sin * 5;
                this.x -= this.vx;
                break;
            case "grunt3":
                sin = Math.sin(this.a);
                this.y += sin * 5;
                this.x -= this.vx;
                break;
           	case "alexandre":
           		alexMoves(this);
                break;
            case "tumtum":
            	tumtumMoves(this);
                break;
            case "rollerqueen":
            	rollerMoves(this);
                break;
            case "rovanelli":
            	rovanMoves(this);
            	break;
            case "rovansat":
            	this.aa += this.ava;
            	rovansatMoves(this);
            	break;
            case "jason":
            	brotherMoves(this);
            	break;
            case "travis":
            	brotherMoves(this);
            	break;
        }

        /* don't bother drawing under the following conditions */
        if (this.x < 0) {
            //dispatch event to remove offscreen enemy
            this.remove(canvas);
            return;
        } else if (this.x > canvas.width) {
            //don't draw yet-to-be-onscreen enemies
            self.onscreen = false;
            return;
        }

        this.onscreen = true;
        var ypos = getFrame();
        if (this.exploding && this.image.index === this.image.frameMax-1) {
            this.remove(canvas);
            return;
        }

        ctx.drawImage(this.image,this.image.xpos,ypos,this.image.width,this.image.height,this.x, this.y, this.width, this.height);

		var num = this.onesImage;
		updateNumber(this,"ones");
		setColor(num);
		ctx.drawImage(num,num.xpos,num.ypos,num.width,num.height, this.x+num.xoffset, this.y+num.yoffset, num.scalew, num.scaleh);

		if (this.tensImage !== null) {
			num = this.tensImage;
			updateNumber(this,"tens");
			setColor(num);
			ctx.drawImage(num,num.xpos,num.ypos,num.width,num.height, this.x+num.xoffset, this.y+num.yoffset, num.scalew, num.scaleh);
		}

    };

    this.remove = function(canvas) {
        clearInterval(fireInterval);
        var evt = new CustomEvent('killMe', {
           detail: {
               id: this.id,
               type: this.type
           },
           bubbles: false,
           cancelable: true
       });
       canvas.dispatchEvent(evt);
       return;
    };

	this.shoot = function(enemy) {
        if ( enemy.onscreen ) {
            rachelnponce.spacepatrol.game.fire(enemy);
        }
        return;
	}

    this.clearShoot = function() {
        clearInterval(fireInterval);
    }

	this.hit = function(damage) {
		health -= damage;
		//gives player feedback registering a hit when enemy has high hp.
		if (health > 0) {
			var hitSound = rachelnponce.spacepatrol.game.getSoundAssets('hit2Sound');
			hitSound.volume = rachelnponce.spacepatrol.game.getEffectVolume();
			hitSound.play();
		}
		return health;
	}

    this.explode = function() {
        this.exploding = true;
        var image = explodeImage;
        image.xpos = 0;
        image.height = 15;
        image.width = 15;
        image.index = 0;
        image.frameMax = 4;
        this.image = image;

        explodeSound = rachelnponce.spacepatrol.game.getSoundAssets('explodeSound');
        explodeSound.volume = rachelnponce.spacepatrol.game.getEffectVolume();
        explodeSound.play();
        if (this.color === "red") {
            badSound = rachelnponce.spacepatrol.game.getSoundAssets('badhitSound');
            badSound.volume = rachelnponce.spacepatrol.game.getEffectVolume();
            badSound.play();
        } else {
            //badSound = rachelnponce.spacepatrol.game.getAssets('diehitSound');
            explodeSound.volume = rachelnponce.spacepatrol.game.getEffectVolume()*1.2;
        }
        //badSound.volume = rachelnponce.spacepatrol.game.getEffectVolume();
        //badSound.play();
    }

	function alexMoves(obj) {
		if (obj.y < 10) {
			obj.dir = 1;
		}
		if (obj.y > canvas.height-obj.height) {
			obj.dir = -1;
		}
		obj.y += obj.vy * obj.dir;
	}

	function tumtumMoves(obj) {
		obj.steps++;
		var col = false;
		var row = false;
		//circles at start position
		if (obj.steps < 250 ) {
			obj.x += 3.4 * Math.cos(obj.a);
        	obj.y += 3.4 * -Math.sin(obj.a);
        	return;
		}

		if (obj.steps === 250) {
			obj.phase = 1;
		}
		//moves to bottom of screen
		if (obj.phase === 1 ) {
			if (obj.y < 230) {
				 obj.y += obj.vy;
			} else { row = true; }
			if (obj.x > 500) {
				obj.x -= obj.vx;
			} else { col = true };
		}

		if (col && row) {
			obj.phase++;
			col = false;
			row = false;
		}
		//circles
		if (obj.steps >=250 && obj.steps < 550) {
				obj.x += 3.4 * Math.cos(obj.a);
				obj.y += 3.4 * -Math.sin(obj.a);
				return;
		}

		if (obj.steps === 550) {
			obj.phase = 2;
		}
		//moves to top of screen
		if (obj.phase === 2) {
			if (obj.y > 70 ) {
				 obj.y -= obj.vy;
			}  else { row = true; }
			if (obj.x > 500) {
				obj.x -= obj.vx;
			} else { col = true };
		}
		//circles
		if (obj.steps >= 550) {
				obj.x += 3.4 * Math.cos(obj.a);
				obj.y += 3.4 * -Math.sin(obj.a);
		}

		if (obj.steps > 750 ) {
			obj.steps = 0;
			obj.phase = 0;
		}
	}

	function rollerMoves(obj) {
		//pick y position based on player position
		if (obj.phase === 0) {
			var _player = rachelnponce.spacepatrol.game.getPlayer();
			obj.newY = _player[1];
			obj.phase++;
		}
		//goto the selected y position
		if (obj.phase === 1) {
			if (obj.y > obj.newY-10 && obj.y < obj.newY+10) {
				obj.phase++;
			} else if (obj.y < obj.newY) {
				obj.y += obj.vy;
			} else if (obj.y > obj.newY) {
				obj.y -= obj.vy
			}
		}
		//try to ram the player
		if (obj.phase === 2) {
			obj.x += obj.vx * obj.dir;
			if (obj.x < 10) {
				obj.dir = 1;
			}
			if (obj.x > canvas.width-obj.width) {
				obj.dir = -1;
				obj.phase = 0;
			}
		}
	}

	function rovanMoves(obj) {
		if (obj.x < 380) {
			obj.dir = 1;
		} else if (obj.x > 550) {
			obj.dir = -1;
		}
		obj.x += obj.vx * obj.dir;
        obj.y += 4 * -Math.cos(obj.a);
	}

	function rovansatMoves(obj) {
		//we're faking the satellite's ability to track the parent object
		rovanMoves(obj);
		//now hover around the new position
		obj.x += 3.4 * Math.cos(obj.aa);
        obj.y += 3.4 * -Math.sin(obj.aa);
	}
	
	function brotherMoves(obj) {
		if (obj.x < 380) {
			obj.dir = 1;
		} else if (obj.x > 550) {
			obj.dir = -1;
		}
		if (obj.y < 40) {
			obj.ydir = 1;
		} else if (obj.y > 360) {
			obj.ydir = -1;
		}
		
		obj.x += obj.vx * obj.dir;
        obj.y += 4 * -Math.cos(obj.a) * obj.ydir;

	}

    return this;
};