var SoundLoader = (function() {
	var self = this;
	this.resources = [];
	this.codec = "";
	this.audioAdded = [];
	this.retrys = 5;
	this.loaded = false;
	this.callback = (function(){console.log("No callback");});
	
	init = (function() {
		//test to see what codecs are supported
		//we'll only attempt to load playable files
		var audioTest = new Audio();
		var codecs = {
		  mp3: !!audioTest.canPlayType('audio/mpeg;').replace(/^no$/,''),
		  ogg: !!audioTest.canPlayType('audio/ogg; codecs="vorbis"').replace(/^no$/,''),
		};	
		
		if (codecs.ogg) {
			self.codec = "ogg";
		} else if (codecs.mp3) {
			self.codec = "mp3";
		} else {
			console.log("audio not supported");
		}
		
	})();

	this.addFiles = function(shortname, pathogg, pathmp3) {
		var newResource = {
			"name": shortname, 
			"ogg": pathogg, 
			"mp3": pathmp3 };
		self.resources.push(newResource);
	};
	
	this.getFile = function(shortname) {
		//returns the audio element to the calling script
		for (var i=self.audioAdded.length-1; i>=0; i--) {
			if (self.audioAdded[i].id === shortname) {
				return self.audioAdded[i];
			}
		}
		console.log("file was not found");
	};
	
	this.startLoad = function() {
		//starts inserting added files to the document
		for (var i = self.resources.length-1; i >= 0; i--) {
			var soundFile = document.createElement("audio");
			soundFile.id = self.resources[i].name;
			soundFile.autoplay = false;
	    	soundFile.preload = false;
	    	soundFile.setAttribute('loaded','false');
	    	var src = document.createElement("source");
	    	if (self.codec === "ogg") {
	    		src.src = self.resources[i].ogg;
	    	} else {
	    		src.src = self.resources[i].mp3;
	    	}
	    	soundFile.appendChild(src);
	    	self.audioAdded.push(soundFile);
	    	//listen for our success or failure
	    	soundFile.addEventListener("canplaythrough", self.onLoad, true);
	    	soundFile.addEventListener("error", self.onError, true);
		}	
	};

	this.onLoad = function(e) {
		//set the currently loaded file to true
		e.target.setAttribute('loaded','true');
		//execute the callback when all files are loaded
		if (self.check()) {
			self.callback();	
		}
	};
	
	this.check = function(target) {
		for (var i = self.audioAdded.length-1; i >= 0; i--) {
			if (self.audioAdded[i].getAttribute('loaded') == 'false'){
				return false;	
			}
		}
		self.loaded = true;
		return true;	
	};
	
	this.on = function(condition,callback) {
		//lets calling script determine a callback function
		switch (condition) {
			case "finish":
				self.callback = callback;
				break;
			default:
				//error	
		}
	};
	
	this.onError = function(e) {
		//trys to load the file again if the attempt fails
		console.log (e.target);
	    self.retrys--;

        if(self.retrys > 0) {
            e.target.load();
        } else {
            self.loaded = true;

            soundFile.removeEventListener("canplaythrough", onLoad, true);
            soundFile.removeEventListener("error", onError, true);

            alert("A sound has failed to loaded");
        }
	};
	
});



