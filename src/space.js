/*************************
 ***** SPACE PATROL! *****
 * Adventures in Numbers *
 *************************
 * v. 1.1-beta-1
 * an html5/canvas game
 * by Rachel Ponce
 * August 15, 2013
 *************************/
var rachelnponce = {
	spacepatrol: {}
};

rachelnponce.spacepatrol.game = (function() {
    var viewport, canvas, ctx, h, w, textOverlay, dialogOverlay,
        gameListener, updater, keyUp, keyDown, isLoaded, state,
        canvases, hud, score, scoreDisplay, healthDisplay, multiplierDisplay,
        player, bullets, enemies, wave, level,
        numArray, perfectScore, currentScore, playerPerformance,
        countdown, countdownStart, failCount,
        storyBuilder, bossFight, bossReveal,
        myLoader, soundLoop, soundEffect, toggleSwitch;
	var self = this;
    var newGame = true;
    var WIDTH = 600;
    var HEIGHT = 360;
    var FRAME_RATE = 1000/40;
    var MUSIC_VOLUME = 0.6;
    var EFFECT_VOLUME = 0.8;

    function init() {
        state = "waiting"; /* states: "waitingForPlayer", "fighting", "waiting" */

    	viewport = document.getElementById("viewport");
        keyDown = [];
        enemies = [];
        bullets = [];
        canvases = [];
        numArray = [];
        playerPerformance = [];

        wave = 0;
        level = 0;
        score = 0;
        bossFight = false;
        failCount = 0;
        buildHUD();
        gameListener = document.createElement('listener');
        gameListener.addEventListener('storyNext', buildStory, false);
        gameListener.addEventListener('fail', buildStory, false);
        gameListener.addEventListener('fail2', buildStory, false);
		storyBuilder = new rachelnponce.spacepatrol.Story(gameListener);
		mainMenu();
        isLoaded = false;
        preloadAssets();
        toggleSwitch = false;
    }

    function preloadAssets() {
        
        mySoundLoader = new SoundLoader();
        mySoundLoader.on('finish', onReady);
        mySoundLoader.addFiles('loopSound','sounds/shapeshift_loop.ogg','sounds/shapeshift_loop.mp3');
        mySoundLoader.addFiles('explodeSound','sounds/explosion.ogg','sounds/explosion.mp3');
        mySoundLoader.addFiles('laser1Sound','sounds/laserplayer.ogg','sounds/laserplayer.mp3');
        mySoundLoader.addFiles('laser2Sound','sounds/laserbad.ogg','sounds/laserbad.mp3');
        //myLoader.addFiles('hit1Sound*:sounds/playerhit.ogg||sounds/playerhit.mp3');
        mySoundLoader.addFiles('hit1Sound','sounds/ouchhit.ogg','sounds/ouchhit.mp3');
        mySoundLoader.addFiles('hit2Sound','sounds/enemyhit.ogg','sounds/enemyhit.mp3');
        mySoundLoader.addFiles('badhitSound','sounds/badhit.ogg','sounds/badhit.mp3');
        //myLoader.addFiles('diehitSound*:sounds/diehit.ogg||sounds/diehit.mp3');*/
        mySoundLoader.startLoad();
        
        myLoader = new html5Preloader();        
        myLoader.addFiles('farback*:images/farback.gif');
        myLoader.addFiles('playerShip*:images/ship_sprites.png');
        myLoader.addFiles('enemies1*:images/enemy_sprites1.png');
        myLoader.addFiles('enemies2*:images/enemy_sprites2.png');
        myLoader.addFiles('enemies3*:images/enemy_sprites3.png');
        myLoader.addFiles('explosion*:images/explosion_sprites.png');
        myLoader.addFiles('numbers*:images/number_sprites.png');
        myLoader.addFiles('boss1*:images/boss1.png');
        myLoader.addFiles('static*:images/static.gif');
        myLoader.on('finish', onReady);
        myLoader.on('error', function(e){ console.error(e); });
        myLoader.on('error', function(){ alert("Sorry, there was an error loading game assets."); });
    }    
    
    function onReady() {
    	console.log(mySoundLoader.loaded);
    	console.log(myLoader.active);
    	//see what myLoader returns....
    	if (mySoundLoader.loaded && !myLoader.active) {
    		addStart();
    	}
    }

    function addStart() {
        isLoaded = true;
        soundLoop = mySoundLoader.getFile('loopSound');
        soundLoop.audible = true;

        var _startButton = document.getElementById('start');
        _startButton.innerHTML = "Start the Adventure";
        _startButton.onclick = buildStory;
    }


    /*** BUILD SCREENS ***/

    function mainMenu() {
        textOverlay = document.getElementById("textOverlay");
        textOverlay.innerHTML = "<h2>SPACE PATROL!</h2><h3>Adventures in Numbers</h3>";
        textOverlay.innerHTML += "<p class='instructions'>Use wasd or arrow keys to move. Hit the spacebar to shoot.</p>";
        var _startButton = document.createElement("a");
        _startButton.id = "start";
        _startButton.innerHTML = "Loading";
        textOverlay.appendChild(_startButton);
        return;
    }

    function startGame() {
        //create the canvases
    	farStarfield = createCanvas("farStarfield");
        farStarfield.image = attachFarImage(farStarfield, myLoader.getFile('farback'));
    	nearStarfield = createCanvas("nearStarfield");
    	canvas = createCanvas("main");
    	canvas.stopMe = true;
    	//storing these in an array makes life easier when we want to hide these
    	canvases.push(farStarfield, nearStarfield, canvas);

    	//check if canvas is supported
    	function isSupported() {
    		return !!(canvas.getContext && canvas.getContext('2d'));
    	}

    	if (!isSupported()) {
    		viewport.innerHTML = "Sorry, your browser does not support canvas :-(";
    		throw new Error('Javascript aborted');
    	} else {
    		startLevel();
    	}

    	//add the dialog box
    	dialogOverlay = document.createElement('div');
        dialogOverlay.id = "dialog";
        viewport.appendChild(dialogOverlay);
        dialogOverlay.style.display="none";

        //set the base volume levels
        soundLoop.volume = MUSIC_VOLUME;
        soundLoop.audible = true;
        soundEffect = {};
        soundEffect.volume = EFFECT_VOLUME;
        soundEffect.audible = true;

        //initialize
        playerPerformance.length = 0;

        //start the updater
        updater = setInterval(function() { updateGame(); }, FRAME_RATE);
    }

    function buildHUD() {
        var content = document.getElementById("content");
        hud = document.createElement('div');
        hud.id = "hud";
        content.insertBefore(hud, viewport);

        scoreDisplay = document.createElement('p');
        scoreDisplay.id = "score";
        hud.appendChild(scoreDisplay);
        scoreDisplay.innerHTML = "Score: " + score;
        scoreDisplay.style.visibility="hidden";

        healthDisplay = document.createElement('p');
        healthDisplay.id = "health";
        hud.appendChild(healthDisplay);
        healthDisplay.innerHTML = "Health: ";
        healthDisplay.style.visibility="hidden";

        multiplierDisplay = document.createElement('p');
        multiplierDisplay.id = "levelMultiplier";
        hud.appendChild(multiplierDisplay);
        multiplierDisplay.innerHTML = "Destroy multiples of ";
        multiplierDisplay.style.visibility="hidden";

        musicControl = document.createElement('a');
        musicControl.setAttribute('class', 'button');
        musicControl.id = 'musicControl';
        hud.appendChild(musicControl);
        musicControl.innerHTML = "press 'q' to toggle music";
        musicControl.onclick = musicToggle;

        soundControl = document.createElement('a');
        soundControl.setAttribute('class', 'button');
        soundControl.id = "soundControl";
        hud.appendChild(soundControl);
        soundControl.innerHTML = "press 'z' to toggle sound effects";
        soundControl.onclick = soundToggle;
    }

    function buildStory(e) { //e is an optional parameter
        state = "waitingForPlayer";

		//hide some things
        healthDisplay.style.visibility="hidden";
        multiplierDisplay.style.visibility="hidden";

        //is it a new level, a failure,
        //or a continuation of previous text?
        if (e && e.type === "storyNext") {
            text = "level"+level;
        } else if (e && e.type === "fail") {
            text = "fail";
        } else if (e && e.type === "fail2") {
            text = "fail2";
        } else {
            var text = "level"+level;
            multiplierDisplay.style.visibility="visible";
        }

        if (level < 0 ) { level = 0 }; //level cannot be less than 0
        if (level === 10) { state = "waiting"; }  //kill the game here

        //update the text on the screen & include a start button
    	storyBuilder.printText(text);

		if (state !== "waiting") {
	    	var goButton = document.createElement("a");
	        goButton.id = "start";
	        goButton.innerHTML = "Let's go!";
	        textOverlay.appendChild(goButton);

	        if (newGame) {
	            newGame = false;
	            multiplierDisplay.style.visibility="hidden";
	            goButton.onclick = startGame;
	        } else {
	        	multiplierDisplay.innerHTML = "You got " + calculateResults() + " correct!";
	            goButton.onclick = startLevel;
	        }
		}
		console.log(state);
    }

    function startLevel() {
        state = "fighting";
		canvas.stopMe = false;

        //initalize some variables
        var starfieldUpdateSpeed = 50;
        bossReveal = false;
        countdownStart = false;
        numArray.length = 0;
        currentScore = 0;
        keyDown.length = 0;

        //laying down some sweet tunes
        soundLoop.currentTime = 0;
        soundLoop.loop = true;
        soundLoop.play();

		//make sure canvases are visible
		for (c in canvases) {
    		canvases[c].style.display="block";
    	}
        scoreDisplay.style.visibility="visible";
        healthDisplay.style.visibility="visible";
        if (level > 0) {
        	multiplierDisplay.style.visibility="visible";
        }

        //draw canvases backgrounds
        farStarfield.velocity = 2;
   		moveFarStarfield = setInterval(function() {backScroll(farStarfield);},100);
   		farStarfield.stop = function () { clearInterval(moveFarStarfield) };

        nearStarfield.velocity = 12;
        nearStarfield.opacity = 1;
        nearStarfield.slowing = false;
        nearStarfield.stars = buildStars(42, nearStarfield, true);	//first arg = no. stars to create
        moveNearStarfield = setInterval(function() {starScroll(nearStarfield);}, starfieldUpdateSpeed);
        nearStarfield.slow = function () { nearStarfield.slowing = true; };
        nearStarfield.stop = function () { clearInterval(moveNearStarfield); };

        canvas.focus();
        canvas.addEventListener('keydown', keyHandler, false);
        canvas.addEventListener('keyup', keyClear, false);
        ctx = canvas.getContext('2d');
        h = canvas.height;
        w = canvas.width;
        canvas.stop = function() { canvas.stopMe = true; };

    	//position the player
        player = new rachelnponce.spacepatrol.Player(w,h);
        player.y = (h/2)-player.height; //midscreen
        player.x = (w - player.width)/2;

		//load the level data enemies
		enemies = generateLevel(rachelnponce.spacepatrol.Levels(level));

		//add event listeners
        canvas.addEventListener("killMe", removeEnemy);

        //if player has more than initial health, they keep it
        //otherwise, start each round with default health
        if (player.updateHealth(0) > 3) {
            var health = player.updateHealth(0);

        } else { var health = player.resetHealth(); }
        healthDisplay.innerHTML = "Health: " + health;

        multiplierDisplay.innerHTML = "Destroy multiples of "+(level+1);

    }


	/*** BUILD BACKGROUNDS ***/

    function createCanvas(id) {
        var canvas = document.createElement('canvas');
        canvas.id = id;
        canvas.tabIndex="-1";
        canvas.width = WIDTH;
        canvas.height = HEIGHT;
        viewport.appendChild(canvas);
        canvas.clear = function () {
            context = canvas.getContext('2d');
            context.clearRect(0,0,canvas.width,canvas.height);
        }
        return canvas;
    }

    function buildStars(n, canvas, firstTime) {
        var starArray =[];
        var ctx = canvas.getContext('2d');
        var _temp = {};
        for (i=0;i<n;i++) {
            var o = canvas.opacity;
            var r = (Math.random()*3)+1;    //radius
            var x = canvas.width+15;
            if (firstTime) {
                x = Math.floor(Math.random()*canvas.width);
            }
            var y = Math.floor(Math.random()*canvas.height);
            _temp = new rachelnponce.spacepatrol.Stars(canvas.velocity);
            _temp.x = x;
            _temp.y = y;
            _temp.radius = r;
            _temp.opacity = o;
            _temp.draw(ctx);
            starArray.push(_temp);
        }
        return starArray;
    }

    function attachFarImage(canvas, loadedImage) {
        var _ctx = canvas.getContext('2d');
        var image = loadedImage;
    	image.height = 600;
    	image.width = 1782;
    	image.dist = (image.width - canvas.width);
    	image.xpos = 0;
    	image.ypos = 0;

        _ctx.drawImage(image,image.xpos,image.ypos,image.width,image.height);
    	return image;
    }

    function starScroll(canvas) {
        var ctx = canvas.getContext('2d');
        ctx.clearRect(0,0,canvas.width, canvas.height);

		var allDone = true;
       	for (var _star in canvas.stars) {
            star = canvas.stars[_star];
            if (nearStarfield.slowing) {
            	if (star.vx <= 0 ) {
            		star.vx = 0;
            	} else {
            		allDone = false;
                	star.vx -= 0.25;
            	}
            }
            star.update(canvas.width);
            star.draw(ctx);
        }

        if (nearStarfield.slowing && allDone) {
        	clearInterval(moveNearStarfield);
        	farStarfield.stop();
        	bossDialog();
        }

    }

    function backScroll(canvas) {
        var ctx = canvas.getContext('2d');
        // adjust scroll speed roughly to length of level
        var _speed = rachelnponce.spacepatrol.Levels(level).length * 250;
        var image = canvas.image;
        image.xpos -= image.dist/_speed;
        // stop scrolling at the end of the level
        if (image.xpos < -(image.width - canvas.width)) {
            clearInterval (canvas.moveFarStarfield );
        } else {
            ctx.drawImage(image,image.xpos,image.ypos,image.width,image.height);
        }
    }


	/*** HANDLE PLAYER INPUT ***/

    function keyHandler(e) {
		keyDown[e.keyCode] = true;
		e.preventDefault();
		return false;
	}

    function keyClear(e) {
       keyDown[e.keyCode] = false;	//clear the key array
    }


    /*** ENEMY MANAGEMENT ***/

    function generateLevel(lvl) {
        enemies = lvl;
        for (var _wave in lvl) {
            for (var _enemy in lvl[_wave]) {
                var enemyObj = new rachelnponce.spacepatrol.Enemy(lvl[_wave][_enemy], canvas);
                if (_wave < lvl.length-1) {
					numArray.push(enemyObj.integer);	//store the random integers for each enemy, excluding bosses
                }
                if (_enemy > 0) {
                    enemyObj.x += _enemy * enemyObj.offset;    //offset creates spacing btwn ships
                }
                enemyObj.id = _enemy;
                enemies[_wave][_enemy] = enemyObj;
            }
        }
        //calculate the perfect score for this level, excluding bosses
        perfectScore = 0;
        for (var _number in numArray) {
        	if (numArray[_number] === 0 || (numArray[_number]%(level+1) === 0 )) {
        		perfectScore+=10;
        	}
        }
        return enemies;
    }

    function waveEmpty() {
        //is the wave empty now?
        for (var i=0; i <= enemies[wave].length; i++) {
            if (enemies[wave][i] != null) {
                return false;
            }
        }
        return true;
    }

    function newWave() {
        wave++;
    }

    function removeEnemy(e) {
        //if it was a boss, go to next level
        if (enemies[wave][e.detail.id].type === "boss" && !player.dead) {
        	delete enemies[wave][e.detail.id];
        	enemies[wave][e.detail.id] = null;
        	if (waveEmpty()) {
        		//state = "waiting";	//immediately stop updater from being called
            	canvas.removeEventListener("killMe", removeEnemy);
            	setTimeout(nextLevel, 800);
        	}
            return;
        }

        delete enemies[wave][e.detail.id];
        enemies[wave][e.detail.id] = null;

        //if the wave is empty, initiate new wave
        if (waveEmpty() && state === "fighting") {
            setTimeout(newWave, 2000);
        }
    }


	/*** GAME LOOP ***/

	function updateGame() {

		//check player's actions
        var action = rachelnponce.spacepatrol.keyHandler(keyDown);

        if ((action.indexOf('music') > -1) && !toggleSwitch ) {
        	musicToggle();
        	toggleSwitch = true;
        	setTimeout(function() { toggleSwitch = false; }, 250);
        }

        if ((action.indexOf('sound') > -1) && !toggleSwitch) {
        	soundToggle();
        	toggleSwitch = true;
        	setTimeout(function() { toggleSwitch = false; }, 250);
        }

        if (state === "fighting") {
        	moveStuff(enemies);
        	drawCanvas(action);
        }

        if ((state === "waitingForPlayer") && (action.indexOf('advance') > -1)) {
            player.delay(550);  //don't shoot when advancing dialog
       		if (bossFight) {
        		onBossFight();
       		}
        }

	}

    function moveStuff(_enemies) {

       var bullets = rachelnponce.spacepatrol.bulletManager.getBullets();

        //checks for death or impending death, delayed to show explosion animation
        if (countdownStart) {
            countdown--;
            if (countdown <= 0) {
                youDie();
                return;
            } else {
                return;
            }
        }

        //checks for boss battle
        if (wave === enemies.length-1 && !bossFight) {
            rachelnponce.spacepatrol.bulletManager.clearAll();
        	state = "waiting";
        	bossFight = true;
        	canvases[1].slow();
        }

       //check for enemy collisions of all types
       for (var _enemy in _enemies[wave]){

            if ((_enemies[wave][_enemy] != null) &&
                (!_enemies[wave][_enemy].exploding) &&
                (_enemies[wave][_enemy].onscreen == true)) {
                //check if enemy collides with player
                var hit = checkCollision(player,_enemies[wave][_enemy]);
                if (hit) {
                    collisionOccurs(hit);
                    return;
                }
                //now check if enemy collides with a player's bullet for the first time
                for (_bullet in bullets) {
                    if (bullets[_bullet].origin === "player" && !bullets[_bullet].hit) {
                        var e_hit = checkCollision(bullets[_bullet],_enemies[wave][_enemy]);
                        if (e_hit) {
                        	//don't let the bullet hit anyone again
                        	bullets[_bullet].hit = true;
                            return;
                        }
                    }
                }
            }
       }

       //check if player collides with enemy bullet
       for (_bullet in bullets) {
      	 if (bullets[_bullet].origin === "enemy" && !bullets[_bullet].hit) {
      	 	var p_hit = checkCollision(player, bullets[_bullet]);
      	 	if (p_hit) {
                collisionOccurs(p_hit);
                bullets[_bullet].hit = true;
                colorToggle('red');
                return;
            }
      	 }
       }
    }

    function drawCanvas(action) {

        //clears the last frame
        ctx.clearRect(0,0,canvas.width, canvas.height);

        //get player's new position & animate
        var position = player.update(action);
        player.x = parseFloat(position[0]);
        player.y = parseFloat(position[1]);
        var frame = player.getFrame();
        ctx.drawImage(player.image,player.image.xpos,frame,player.image.width,player.image.height,player.x, player.y, player.width, player.height);

        //get current enemies' new positions and draw
        for(var _wave in enemies) {
            if (_wave == wave) {
                for (var _enemy in enemies[_wave]) {
                    if (enemies[_wave][_enemy] !== null) {	//Uncaught TypeError: Cannot read property '1' of undefined on last level
                        enemies[_wave][_enemy].draw(canvas);
                    }
                }
            }
        }

        //now draw all the bullets
        rachelnponce.spacepatrol.bulletManager.update(canvas);

    }


	/*** STATE MANAGEMENT ***/

    function clearLevel() {
    	//change the game state
    	state = "waiting";

        //stop the music
        soundLoop.pause();

        //clear the canvases & stops the updater interval
        for (var _c in canvases) {
            canvases[_c].stop();
            canvases[_c].clear();
    		canvases[_c].style.display="none";
    	}

         //clear out any old bullets & enemies
        rachelnponce.spacepatrol.bulletManager.clearAll();
        wave = 0;
        for (array in enemies) {
            for (obj in enemies[array]) {
                if (enemies[array][obj] != null) {
                    enemies[array][obj].clearShoot();
                }
            }
        }
        enemies.length = 0;
		bossFight = false;
        //restart background scrolling
        farStarfield.image.xpos = 0;
    }

	function nextLevel() {
        clearLevel();

        //build new level
        failCount = 0;
        level++;
        buildStory();
    }

    function failLevel() {
        failCount++;
        if (failCount === 1) {
            var evt = new CustomEvent("fail")
        } else if (failCount === 2) {
            failCount = 0;
            if (level > 0) {
                level--;
                var evt = new CustomEvent("fail2");
            } else {
                var evt = new CustomEvent("fail");
            }
        }
        gameListener.dispatchEvent(evt);
    }

    function bossDialog() {
		state = "waitingForPlayer";
		dialogOverlay.innerHTML = "";	//clear out any previous text
		var _image = null;
        var _txt = "level"+level+"boss";
        var _storyData = storyBuilder.returnText(_txt);
		var noStatic = true;
        if (_storyData[1] != null) {
        	_image = _storyData[1];
        	noStatic = false;
        } else {
        	_image = getAssets('static');
        }

        console.log(_image);
        _image.width = 150;
        _image.height = 150;
        _image.id = 'mugshot';
        dialogOverlay.appendChild(_image);    //boss image
        _image.setAttribute('class', 'mugshot');


        if (!noStatic) {
        	_image.style.visibility = "hidden";
        	var _static = getAssets('static');
        	_static.width = 150;
        	_static.height = 150;
        	_static.id = 'static';
        	dialogOverlay.appendChild(_static);
        	setTimeout( function() { document.getElementById('mugshot').style.visibility = "visible"; }, 1200);
        }
        /*
        _static.width = 150;
        _static.id = 'static';
        dialogOverlay.appendChild(_static);
        //_static.setAttribute('class', 'static');
        */
        dialogOverlay.innerHTML += _storyData[0];  //text
        dialogOverlay.style.display="block";

    }

	function onBossFight() {
		state = "fighting";
		dialogOverlay.style.display="none";
        for (var _bosses in enemies[wave]) {
        	enemies[wave][_bosses].reveal = true;
        }
	}


	/*** COLLISIONS ***/

    function checkCollision(object1,object2) {
    	//if checking player, player should be the first argument
        //	obj1 should be either player or bullet
        //	obj2 should be bullet or enemy

        var obj1 = {};
        var obj2 = {};
        var hit = false;

        obj1.x = object1.x;
        obj1.y = object1.y;
        obj1.width = object1.width;
        obj1.height = object1.height;

        obj2.x = object2.x;
        obj2.y = object2.y;
        obj2.width = object2.width;
        obj2.height = object2.height;

        //offsets player to correct for thruster animation
        if (object1.type === "player") {
            obj1.x += 24;
            obj1.width -= 24;
        }

        //check for overlapping boundaries & register hit
        if ((obj1.x + obj1.width > obj2.x) &&
            (obj1.x < obj2.x + obj2.width) &&
            (obj1.y < obj2.y + obj2.height) &&
            (obj1.y + obj1.height > obj2.y)) {
            hit = true;
        } else {
            return false;
        }

        //bullet hits enemy
        if (hit && object1.type === "bullet") {
		    object1.remove(canvas);
	        var hitpoints = object2.hit(player.power);
	        if (hitpoints <= 0) {
                calculateScore(object2);
	        	object2.explode(canvas);
	        	//update score when enemy is destroyed

	        }
	        return object2;
        }

        //bullet hits player
        if (hit && object1.type === "player") {
           if (object2.type === "bullet") {
               object2.remove(canvas);
           } else {
               object2.explode(canvas);
           }
			return object2;
		}
    }

    function collisionOccurs(obj) {
         //what type is it and how does it effect the player?
         //someday may create power-ups for player
        if (obj.effect === "damage") {
             var health = player.updateHealth(-obj.pain);
             if (health <= 0) {
             	player.dead = true;
                player.explode();
                countdownStart = true;
                countdown = 25;
             } else {
                healthDisplay.innerHTML = "Health: " + health;
             }
        }
    }


	/*** GETTERS & PUBLIC FUNCTIONS ***/

    function youDie() {
    	//reset score to points earned when starting level
    	score -= currentScore;
    	scoreDisplay.innerHTML = "Score: " + score;
    	//reset the level
        clearLevel();
        failLevel();
    }

    function fire(obj) {
        var shooter = { };
        var origin = "";
        var laserSound = getSoundAssets('laser2Sound');	//defaults to enemy laser sound
        if (obj.type === "player") {
            origin = "player";
            shooter.x = obj.x + obj.width;
            laserSound = getSoundAssets('laser1Sound');
        } else if (obj.type === "boss") {
        	//get extra parameters?
        	origin = "boss";
            shooter.x = obj.x;
            shooter.name = obj.name;
	    } else {
            origin = "enemy";
            shooter.x = obj.x;
            shooter.name = obj.name;
        }
        shooter.y = obj.y +  obj.height/2;
        shooter.vy = obj.vy;
        laserSound.volume = getEffectVolume();
        laserSound.play();
        //bulletManager will create a new Bullet object;
        rachelnponce.spacepatrol.bulletManager.add(shooter.x, shooter.y, origin, player.x, player.y, shooter.name);
    };

    function getAssets(asset) {
        if (isLoaded) {
            return myLoader.getFile(asset);
        } else {
            throw "asset not loaded";
        }
    }
    
    function getSoundAssets(asset) {
        if (isLoaded) {
            return mySoundLoader.getFile(asset);
        } else {
            throw "asset not loaded";
        }
    }

    function getState() {
    	return state;
    }

	function getLevel() {
		return level;
	}

	function getPlayer() {
		var _position = [];
		_position[0] = player.x;
		_position[1] = player.y;
		return _position;
	}

	function getEffectVolume() {
		return soundEffect.volume;
	}


	/*** MISCELLANEOUS ***/

    function musicToggle() {
    	if (soundLoop.audible) {
    		soundLoop.volume = 0;
    		soundLoop.audible = false;
    	} else {
    		soundLoop.audible = true;
    		soundLoop.volume = MUSIC_VOLUME;
    	}
    }

    function soundToggle() {
    	if (soundEffect.audible) {
    		soundEffect.volume = 0;
    		soundEffect.audible = false;
    	} else {
    		soundEffect.audible = true;
    		soundEffect.volume = EFFECT_VOLUME;
    	}
    }

    function colorToggle(color) {
        if (color === "red") {
            canvas.style.backgroundColor = "red";
            canvas.style.opacity = 0.2;
            setTimeout(function() { colorToggle('reset'); }, 30);
        } else if (color === "yellow") {
            canvas.style.backgroundColor = "yellow";
            canvas.style.opacity = 0.2;
            setTimeout(function() { colorToggle('reset'); }, 30);
        } else {
            canvas.style.backgroundColor = "transparent";
            canvas.style.opacity = 1;
        }
    }

    function overallResults() {
    	/* display playerPerformance; */
    }

	function calculateResults() {
    	var totalEnemies = numArray.length;
    	var enemiesToDestroy = perfectScore/10;
    	var results = (currentScore-100)/10;	// -100 to eliminate boss score.
    	var missed = enemiesToDestroy - results;
	    var resultsFormatted = (totalEnemies-missed)+"/"+totalEnemies;
	    playerPerformance[level] = resultsFormatted;
		return resultsFormatted;
    }

    function calculateScore(obj) {
    	var _integer = obj.integer;
       	var multiple = level + 1;
       	if (_integer%multiple === 0 || isNaN(_integer%multiple)) {
       		obj.color = "green";
           	score += obj.value;
           	currentScore += obj.value;
       	} else {
            colorToggle('yellow');
       		obj.color = "red";
       		score -= obj.value;
       		currentScore -= obj.value;
       		if (score <=0) {
       			score =0;
       		}
       	}

        scoreDisplay.innerHTML = "Score: " + score;
    }


    return {
        init: init,
        getAssets: getAssets,
        getSoundAssets: getSoundAssets,
        getState: getState,
        getLevel: getLevel,
        getEffectVolume: getEffectVolume,
        getPlayer: getPlayer,
        removeEnemy: removeEnemy,
        fire: fire
    };
})();


/* Object for keeping track of the bullets */
rachelnponce.spacepatrol.bulletManager = (function() {
	var bullets = [];

	this.add = function(x, y, origin, pX, pY,name) {
		bullets.push(new rachelnponce.spacepatrol.Bullet(x, y, origin, pX, pY,name));
	}

	this.update = function(canvas) {
		if (bullets.length <= 0) {
			return;
		}

		for (var i=0; i < bullets.length; i++) {
            //remove bullets that go off the canvas or have collided
			if ( (bullets[i].x > canvas.width || bullets[i].x < 0) ||
				(bullets[i].y > canvas.height || bullets[i].y < 0) ||
                bullets[i].removeMe ) {
                bullets[i] = null;
				bullets.splice(i,1);
				i--;
            } else {
				bullets[i].draw(canvas);
			}
		}
	}

	this.getBullets = function() {
        return bullets;
	}

    this.clearAll = function() {
        bullets.length = 0;
    }

	return {
		add: add,
		update: update,
		getBullets: getBullets,
        clearAll: clearAll
	};

})();